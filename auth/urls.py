from django.urls import path
from auth.views import MyObtainTokenPairView, RegisterView, ChangePasswordView, UpdateProfileView
from rest_framework_simplejwt.views import TokenRefreshView

urlpatterns = [
    path('login/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name='auth_register'),
    path('update-password/<int:pk>/', ChangePasswordView.as_view(), name='auth_change_password'),
    path('update-user/<int:pk>/', UpdateProfileView.as_view(), name='auth_update_profile'),
]