from django.apps import AppConfig


class TruelayerConfig(AppConfig):
    name = 'truelayer'
