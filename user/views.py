from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, generics
from django.contrib.auth.models import User
from .serializers import UserSerializer
from rest_framework.response import Response


class UserViewSet(generics.ListCreateAPIView):
    """
    Lists information related to the current user.
    """
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        instance = request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

