from django.urls import path
from .views import UserViewSet

urlpatterns = [
    path('details/', UserViewSet.as_view(), name='user_info'),
]